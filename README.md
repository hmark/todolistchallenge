﻿My version of ToDo challenge.
Well it is bigger than I thought it will be, but, I like CQRS pattern.

If you want to run it, you need to go to configuration folder and change connection string in mssql.config file. 
For the documentation you can visit http://localhost:4000/swagger/index.html here: I implemented Swagger.UI.

For Authentication I used JWT Token.
There are a lot of things that I would fix and add for this project, but as I had only 2 days, this was everything I could manage.
 
