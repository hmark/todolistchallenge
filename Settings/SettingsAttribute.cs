using System;

namespace Settings
{
    [AttributeUsage(AttributeTargets.Class)]
    public class SettingsAttribute : Attribute
    {
        public string FileName { get; set; }
    }
}