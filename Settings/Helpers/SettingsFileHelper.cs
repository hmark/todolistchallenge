using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using JetBrains.Annotations;

namespace Settings.Helpers
{
   public static class SettingsFileHelper
	{
		public static TSettings ReadSettings<TSettings>() where TSettings : new()
		{
			return (TSettings)DoReadSettings(typeof(TSettings), null);
		}

		public static TSettings ReadSettings<TSettings>(string fileName) where TSettings : new()
		{
			return (TSettings)DoReadSettings(typeof(TSettings), fileName);
		}

		public static object ReadSettings(Type settingsType)
		{
			return DoReadSettings(settingsType, null);
		}

		public static object ReadSettings(Type settingsType, string fileName)
		{
			return DoReadSettings(settingsType, fileName);
		}

		private static object DoReadSettings(Type settingsType, [CanBeNull] string fileName)
		{
			fileName = !string.IsNullOrEmpty(fileName) ? fileName : GetSettingsFileName(settingsType);
			try
			{
				var o = CreateSettingsInstance(settingsType, fileName);
				//ValidateSettings(o);
				return o;
			}
			catch (Exception e)
			{
				throw new InvalidOperationException($"Could not read settings from file: {fileName}", e);
			}
		}

		private static object CreateSettingsInstance(Type settingsType, string fileName)
		{
			object result;

			fileName = PatchFilename(fileName);
			if (!File.Exists(fileName))
				result = Activator.CreateInstance(settingsType);
			else
			{
				var configContent = File.ReadAllText(fileName, Encoding.UTF8);
				result = YamlHelper.Deserialize(configContent, settingsType);
			}

			var mySettingsFile = GetMySettingFileName(fileName);
			if (File.Exists(mySettingsFile))
				Merge(settingsType, result, mySettingsFile);
			return result;
		}

		private static  string GetMySettingFileName(string fileName)
		{
			var extension = Path.GetExtension(fileName) ?? "";
			return fileName.Substring(0, fileName.Length - extension.Length) + "-my" + extension;
		}

		private static void Merge(Type settingsType, object result, string mySettingsFile)
		{
			var content = File.ReadAllText(mySettingsFile,Encoding.UTF8);
			var mySettingsObj = YamlHelper.Deserialize<object>(content);
			var mySettings = YamlHelper.Deserialize(content, settingsType);
			MergeObjects(result, mySettings, mySettingsObj);
		}

		private static void MergeObjects(object result, object mySettings, object mySettingsFields)
		{
			var dict = mySettingsFields as Dictionary<object, object>;
			if (dict != null)
				foreach (var kv in dict)
				{
					var prop = (string)kv.Key;
					if (kv.Value is Dictionary<string, object>)
						MergeObjects(GetOrCreatePropertyValue(result, prop), kv.Value, GetPropertyValue(mySettings, prop));
					else
						SetPropertyValue(result, prop, GetPropertyValue(mySettings, prop));
				}
		}

		private static void SetPropertyValue(object o, string p, object v)
		{
			var property = o.GetType().GetProperty(p);
			property.SetValue(o, v, new object[0]);
		}

		private static object GetPropertyValue(object o, string p)
		{
			return o.GetType().GetProperty(p).GetValue(o, new object[0]);
		}

		private static object GetOrCreatePropertyValue(object o, string p)
		{
			var property = o.GetType().GetProperty(p);
			var currentValue = property.GetValue(o, new object[0]);
			if (currentValue == null)
			{
				currentValue = Activator.CreateInstance(property.PropertyType);
				property.SetValue(o, currentValue, new object[0]);
			}
			return currentValue;
		}

		private static string GetSettingsFileName(Type settingsType)
		{
			var fileNameAttribute = settingsType
				.GetTypeInfo()
				.GetCustomAttributes()
				.Cast<SettingsAttribute>()
				.FirstOrDefault();
			return fileNameAttribute?.FileName 
				?? "settings\\" + settingsType.Name;
		}

		public static string PatchFilename(string filename, string baseDirectoryPath = null)
		{
			return Path.IsPathRooted(filename) ? filename : WalkDirectoryTree(filename, File.Exists, baseDirectoryPath);
		}

		public static string PatchDirectoryName(string dirName, string baseDirectoryPath = null)
		{
			return Path.IsPathRooted(dirName) ? dirName : WalkDirectoryTree(dirName, Directory.Exists, baseDirectoryPath);
		}

		private static string WalkDirectoryTree(string filename, Func<string, bool> fileSystemObjectExists, string baseDirectoryPath = null)
		{
			if (baseDirectoryPath == null)
				baseDirectoryPath = AppDomainHelpers.GetBinDirectory();
			var baseDirectory = new DirectoryInfo(baseDirectoryPath);
			while (baseDirectory != null)
			{
				var candidateFilename = Path.Combine(baseDirectory.FullName, filename);
				if (fileSystemObjectExists(candidateFilename))
					return candidateFilename;
				baseDirectory = baseDirectory.Parent;
			}
			return filename;
		}
	}
}