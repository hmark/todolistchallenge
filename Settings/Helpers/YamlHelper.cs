using System;
using System.IO;
using YamlDotNet.Serialization;

namespace Settings.Helpers
{
    public static class YamlHelper
    {
        public static object Deserialize(string s, Type expectedType, bool ignoreUnmatched = true)
        {
            var serializer = new Deserializer(ignoreUnmatched: ignoreUnmatched);
            var reader = new StringReader(s);
            return serializer.Deserialize(reader, expectedType);
        }

        public static T Deserialize<T>(string s, bool ignoreUnmatched = true)
        {
            var serializer = new Deserializer(ignoreUnmatched: ignoreUnmatched);
            var reader = new StringReader(s);
            return serializer.Deserialize<T>(reader);
        }
    }
}