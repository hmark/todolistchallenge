using Microsoft.Extensions.PlatformAbstractions;

namespace Settings.Helpers
{
    public class AppDomainHelpers
    {
        public static string GetBinDirectory()
        {
            return PlatformServices.Default.Application.ApplicationBasePath;
        }
    }
}