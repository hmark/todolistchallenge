namespace Settings.MsSql
{
    [Settings(FileName = "configurations/mssql.config")]
    public class MsSqlSettings
    {
        public string ConnectionString { get; set; }
    }
}