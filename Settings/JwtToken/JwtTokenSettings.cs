namespace Settings.JwtToken
{
    [Settings(FileName = "configurations/jwttoken.config")]
    public class JwtTokenSettings
    {
        public string SekretKey { get; set; }
    }
}