namespace Domain.Services.ToDoList
{
    public class ToDoView
    {
        public string Id { get; set; }
        public string Title { get; set; }
        public string State { get; set; }
    }
}