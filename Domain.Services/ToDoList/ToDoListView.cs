namespace Domain.Services.ToDoList
{
    public class ToDoListView
    {
        public string Id { get; set; }
        public string Title { get; set; }
        public ToDoView[] ToDoViews { get; set; }
    }
}