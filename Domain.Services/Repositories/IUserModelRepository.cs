using System.Collections.ObjectModel;
using System.Threading.Tasks;
using Domain.User;

namespace Domain.Services.Repositories
{
    public interface IUserModelRepository
    {
        Task<ReadOnlyCollection<UserModel>> GetAll();
        Task<UserModel> FindById(string userId);
        Task<UserModel> FindByEmail(string userId);
        Task<UserModel> Add(UserModel userModel);
        Task<UserModel> Update(UserModel userModel);
        Task Remove(UserModel userModel);
    }
}