using System.Collections.ObjectModel;
using System.Threading.Tasks;
using Domain.ToDo;

namespace Domain.Services.Repositories
{
    public interface IToDoListRepository
    {
        Task<ReadOnlyCollection<ToDoListModel>> GetAll();
        Task<ToDoListModel> FindById(string toDoListId);
        Task<ToDoListModel> FindByUserId(string userId);
        Task<ToDoListModel> Add(ToDoListModel toDoListModel);
        Task<ToDoListModel> Update(ToDoListModel toDoListModel);
        Task Remove(ToDoListModel userModel);
    }
}