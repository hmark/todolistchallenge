using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using Domain.Context;
using Domain.User;
using Microsoft.EntityFrameworkCore;

namespace Domain.Services.Repositories
{
    public class UserModelRepository : IUserModelRepository
    {
        private readonly Func<DataContext> createDbContext;

        public UserModelRepository(Func<DataContext> createDbContext)
        {
            this.createDbContext = createDbContext;
        }

        public async Task<ReadOnlyCollection<UserModel>> GetAll()
        {
            using (var db = createDbContext())
            {
                var allUsers = await db.Users
                    .Include(x => x.ToDoList)
                    .Select(x => CreateUserModel(x))
                    .ToArrayAsync();
                return new ReadOnlyCollection<UserModel>(allUsers);
            }
        }
        
        public async Task<UserModel> FindById(string userId)
        {
            using (var db = createDbContext())
            {
                var userEntry = await db.Users
                    .Include(u => u.ToDoList)
                    .SingleOrDefaultAsync(u => u.Id == Guid.Parse(userId));
                return CreateUserModel(userEntry);
            }
        }
        
        public async Task<UserModel> FindByEmail(string userEmail)
        {
            using (var db = createDbContext())
            {
                var userEntry = await db.Users
                    .Include(u => u.ToDoList)
                    .SingleOrDefaultAsync(u => u.Email == userEmail);
                
                return CreateUserModel(userEntry);
            }
        }

        public async Task<UserModel> Add(UserModel userModel)
        {
            using (var db = createDbContext())
            {
                var user = new Entities.User();
                UpdateUserFromModel(user, userModel, db);
                await db.Users.AddAsync(user);
                await db.SaveChangesAsync(); 
                return CreateUserModel(user);
            }
        }

        public async Task<UserModel> Update(UserModel userModel)
        {
            using (var db = createDbContext())
            {
                var user = await db
                    .Users
                    .Include(m => m.ToDoList)
                    .SingleAsync(m => m.Id == Guid.Parse(userModel.Id));
                UpdateUserFromModel(user, userModel, db);
                await db.SaveChangesAsync();
                return userModel;
            }
        }

        public async Task Remove(UserModel userModel)
        {
            using (var db = createDbContext())
            {
                var user = await db
                    .Users
                    .Include(m => m.ToDoList)
                    .SingleAsync(m => m.Id == Guid.Parse(userModel.Id));
                db.Users.Remove(user);
                await db.SaveChangesAsync();
            }
        }

        private void UpdateUserFromModel(Entities.User user, UserModel userModel, DataContext db)
        {
            user.FirstName = userModel.FirstName;
            user.LastName = userModel.LastName;
            user.Email = userModel.Email;
            user.PasswordHash = userModel.PasswordHash;
            user.PasswordSalt = userModel.PasswordSalt;
        }

        private static UserModel CreateUserModel(Entities.User userEntry)
        {
            if (userEntry == null)
            {
                return null;
            }
            return UserModel.Load(
                userEntry.Id.ToString(), 
                userEntry.FirstName,
                userEntry.LastName,
                userEntry.Email,
                userEntry.PasswordHash,
                userEntry.PasswordSalt,
                userEntry.ClaimType,
                userEntry.ToDoList?.ToArray());
            
        }
    }
}