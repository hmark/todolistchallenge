using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using Domain.Context;
using Domain.StaticHelpers;
using Domain.ToDo;
using Domain.User;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Internal;

namespace Domain.Services.Repositories
{
    public class ToDoListRepository : IToDoListRepository
    {
         private readonly Func<DataContext> createDbContext;

        public ToDoListRepository(Func<DataContext> createDbContext)
        {
            this.createDbContext = createDbContext;
        }

        public async Task<ReadOnlyCollection<ToDoListModel>> GetAll()
        {
            using (var db = createDbContext())
            {
                var toDoListModels = await db.ToDoList
                    .Include(x => x.ToDos)
                    .Select(x => CreateToDoListModel(x))
                    .ToArrayAsync();
                return new ReadOnlyCollection<ToDoListModel>(toDoListModels);
            }
        }
        
        public async Task<ToDoListModel> FindById(string toDoListId)
        {
            using (var db = createDbContext())
            {
                var toDoList = await db.ToDoList
                    .Include(u => u.ToDos)
                    .SingleOrDefaultAsync(u => u.Id == Guid.Parse(toDoListId));
                return CreateToDoListModel(toDoList);
            }
        }
        
        public async Task<ToDoListModel> FindByUserId(string userId)
        {
            using (var db = createDbContext())
            {
                var userEntry = await db.ToDoList
                    .Include(u => u.ToDos)
                    .SingleOrDefaultAsync(u => u.UserId == Guid.Parse(userId));
                
                return CreateToDoListModel(userEntry);
            }
        }

        public async Task<ToDoListModel> Add(ToDoListModel toDoListModel)
        {
            using (var db = createDbContext())
            {
                var toDoList = new Entities.ToDoList();;
                UpdateToDoListFromModel(toDoList, toDoListModel);
                await db.ToDoList.AddAsync(toDoList);
                await db.SaveChangesAsync(); 
                return CreateToDoListModel(toDoList);
            }
        }

        public async Task<ToDoListModel> Update(ToDoListModel toDoListModel)
        {
            using (var db = createDbContext())
            {
                var toDoList = await db
                    .ToDoList
                    .Include(m => m.ToDos)
                    .SingleAsync(m => m.Id == Guid.Parse(toDoListModel.Id));
                UpdateToDoListFromModel(toDoList, toDoListModel);
                await db.SaveChangesAsync();
                return toDoListModel;
            }
        }

        public async Task Remove(ToDoListModel userModel)
        {
            using (var db = createDbContext())
            {
                var user = await db
                    .Users
                    .Include(m => m.ToDoList)
                    .SingleAsync(m => m.Id == Guid.Parse(userModel.Id));
                db.Users.Remove(user);
                await db.SaveChangesAsync();
            }
        }

        private void UpdateToDoListFromModel(Entities.ToDoList toDoList, ToDoListModel toDoListModel)
        {
            toDoList.Title = toDoListModel.Title;
            toDoList.UserId = new Guid(toDoListModel.UserId);
            if(toDoListModel.ToDoModels.Any())
            {
                var length = toDoListModel.ToDoModels.Count;
                toDoList.ToDos = new Entities.ToDo[length];
                for (var i = 0; i < length; i++)
                {
                    toDoList.ToDos[i] = new Entities.ToDo
                    {
                        State = toDoListModel.ToDoModels[i].State, Title = toDoListModel.ToDoModels[i].Title
                    };
                }
            }
        }
        
        private ToDoListModel CreateToDoListModel(Entities.ToDoList todoEntry)
        {
            if (todoEntry == null)
            {
                return null;
            }

            ToDoModel[] toDoModels = null;
            if (todoEntry.ToDos.Any())
            {
                toDoModels = todoEntry
                    .ToDos
                    .Select(x =>
                        ToDoModel.Load(
                            x.Id.ToString(),
                            x.Title,
                            x.State,
                            x.ToDoListId.ToString()))
                    .ToArray();
            }
            return ToDoListModel.Load(
                todoEntry.Id.ToString(),
                todoEntry.Title,
                todoEntry.UserId.ToString(),
                toDoModels);
        }

        
    }
}