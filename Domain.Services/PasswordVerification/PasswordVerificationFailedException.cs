using System;

namespace Domain.Services.PasswordVerification
{
    public class PasswordVerificationFailedException : Exception
    {
        public string ErrorCode { get; }

        public PasswordVerificationFailedException(string errorCode) : base($"Password failed verification: {errorCode}")
        {
            ErrorCode = errorCode;
        }
    }
}