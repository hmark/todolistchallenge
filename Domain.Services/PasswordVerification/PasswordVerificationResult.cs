namespace Domain.Services.PasswordVerification
{
    public enum PasswordVerificationResult
    {
        Ok,
        NotMatch,
        InvalidPassword,
        Error
    }
}