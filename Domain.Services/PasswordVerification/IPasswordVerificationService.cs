using System.Threading.Tasks;

namespace Domain.Services.PasswordVerification
{
    public interface IPasswordVerificationService
    {
        Task<PasswordVerificationResult> Verify(string password, byte[] storedHash, byte[] storedSalt);
    }
}