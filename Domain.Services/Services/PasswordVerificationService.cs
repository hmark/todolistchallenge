using System;
using System.Threading.Tasks;
using Domain.Services.PasswordVerification;

namespace Domain.Services.Services
{
    public class PasswordVerificationService : IPasswordVerificationService
    {
        public async Task<PasswordVerificationResult> Verify(string password, byte[] storedHash, byte[] storedSalt)
        {
            if (password == null) return PasswordVerificationResult.Error;
            if (string.IsNullOrWhiteSpace(password)) return PasswordVerificationResult.InvalidPassword;
            if (storedHash.Length != 64) return PasswordVerificationResult.InvalidPassword;
            if (storedSalt.Length != 128) return PasswordVerificationResult.InvalidPassword;
 
            using (var hmac = new System.Security.Cryptography.HMACSHA512(storedSalt))
            {
                var computedHash = hmac.ComputeHash(System.Text.Encoding.UTF8.GetBytes(password));
                for (int i = 0; i < computedHash.Length; i++)
                {
                    if (computedHash[i] != storedHash[i]) return PasswordVerificationResult.NotMatch;
                }
            }
 
            return PasswordVerificationResult.Ok;
        }
    }
}