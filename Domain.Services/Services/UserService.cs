using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Domain.Exceptions;
using Domain.Services.PasswordVerification;
using Domain.Services.Repositories;
using Domain.Services.Token;
using Domain.User;

namespace Domain.Services.Services
{
    public class UserService
    {
        private readonly IPasswordVerificationService passwordVerificationService;
        private readonly IUserModelRepository userModelRepository;
        private readonly ITokenProviderService tokenProviderService;

        public UserService(
            IPasswordVerificationService passwordVerificationService,
            IUserModelRepository userModelRepository,
            ITokenProviderService tokenProviderService)
        {
            this.passwordVerificationService = passwordVerificationService;
            this.userModelRepository = userModelRepository;
            this.tokenProviderService = tokenProviderService;
        }

        public async Task<string> Authenticate(string email, string password)
        {
            if (string.IsNullOrEmpty(email) || string.IsNullOrEmpty(password))
                throw new ArgumentNullException();

            var user = await FindUserModelByEmail(email);
            if (user == null)
                throw new NotFoundException(ResourceType.User);

            var passwordVerificationResult =
                await passwordVerificationService.Verify(password, user.PasswordHash, user.PasswordSalt);
            if (passwordVerificationResult != PasswordVerificationResult.Ok)
            {
                throw new PasswordVerificationFailedException(passwordVerificationResult.ToString());
            }

            // authentication successful
            return tokenProviderService.GenerateToken(user.Id);
        }

        public async Task<string> SaveUser(SaveUserEvent saveUserEvent, string adminId)
        {
            var adminUserModel = await FindUserModelById(adminId);
            if (adminUserModel == null)
            {
                throw new ForbiddenException(ResourceType.User);
            }
            if (!string.IsNullOrEmpty(saveUserEvent.Id))
            {
                var userModel = await FindUserModelById(saveUserEvent.Id);
                if (!adminUserModel.CanUpdate() && !adminId.Equals(userModel.Id))
                {
                    throw new ForbiddenException(ResourceType.User);
                }
                userModel.Update(
                    saveUserEvent.FirstName, 
                    saveUserEvent.LastName,
                    saveUserEvent.Email,
                    saveUserEvent.Password);
                userModel = await userModelRepository.Update(userModel);
                return userModel.Id;
            }
            else
            {
                var userModel = UserModel.Create(
                    saveUserEvent.FirstName, 
                    saveUserEvent.LastName,
                    saveUserEvent.Email,
                    saveUserEvent.Password);
                if (!adminUserModel.CanCreate())
                {
                    throw new ForbiddenException(ResourceType.User);
                }
                userModel = await userModelRepository.Add(userModel);
                return userModel.Id;
            }
        }

        public async Task Remove(string adminId, string userToDeleteId)
        {
            var adminUserModel = await FindUserModelById(adminId);

            if (!adminUserModel.CanRemove() && !adminId.Equals(userToDeleteId))
            {
                throw new ForbiddenException(ResourceType.User);
            }

            var userToDeleteModel = await FindUserModelById(userToDeleteId);
            await userModelRepository.Remove(userToDeleteModel);
        }

        private async Task<UserModel> FindUserModelById(string userId)
        {
            var userModel = await userModelRepository.FindById(userId);
            if (userModel == null)
            {
                throw new NotFoundException(ResourceType.User);
            }

            return userModel;
        }

        private async Task<UserModel> FindUserModelByEmail(string email)
        {
            var userModel = await userModelRepository.FindByEmail(email);
            if (userModel == null)
            {
                throw new NotFoundException(ResourceType.User);
            }

            return userModel;
        }
    }
}