using System.Linq;
using System.Threading.Tasks;
using Domain.Exceptions;
using Domain.Services.Repositories;
using Domain.ToDo;
using Domain.User;

namespace Domain.Services.Services
{
    public class ToDoService
    {
        private readonly IToDoListRepository toDoListRepository;

        public ToDoService(IToDoListRepository toDoListRepository)
        {
            this.toDoListRepository = toDoListRepository;
        }

        public async Task SaveTodoList(SaveToDoListEvent[] saveToDoEvents)
        {
            foreach (var saveToDoEvent in saveToDoEvents)
            {
                var toDoListModel = ToDoListModel.Create(
                    saveToDoEvent.Title,
                    saveToDoEvent.UserId,
                    Convert(saveToDoEvent.ToDoModels.ToArray()));

                await toDoListRepository.Add(toDoListModel);
            }
        }

        public async Task<string> SaveTodoList(SaveToDoListEvent saveToDoEvent)
        {
            if (!string.IsNullOrEmpty(saveToDoEvent.Id))
            {
                var toDoListModel = await FindTodoListModelById(saveToDoEvent.Id);
                if (!toDoListModel.CanUpdate(saveToDoEvent.UserId))
                {
                    throw new ForbiddenException(ResourceType.ToDoList);
                }

                toDoListModel.Update(
                    saveToDoEvent.Title,
                    Convert(saveToDoEvent.ToDoModels.ToArray()));
                toDoListModel = await toDoListRepository.Update(toDoListModel);
                return toDoListModel.Id;
            }
            else
            {
                var toDoListModel = ToDoListModel.Create(
                    saveToDoEvent.Title,
                    saveToDoEvent.UserId,
                    Convert(saveToDoEvent.ToDoModels.ToArray()));

                toDoListModel = await toDoListRepository.Add(toDoListModel);
                return toDoListModel.Id;
            }
        }

        public async Task Remove(string userId, string todoListId)
        {
            var toDoListModel = await FindTodoListModelById(todoListId);

            if (!toDoListModel.CanRemove(userId))
            {
                throw new ForbiddenException(ResourceType.ToDoList);
            }

            await toDoListRepository.Remove(toDoListModel);
        }

        private static ToDoModel[] Convert(SaveToDoEvent[] saveToDoEvents)
        {
            return saveToDoEvents
                .Select(x => ToDoModel.Load(null, x.Title, x.ToDoState, x.ToDoListId))
                .ToArray();
        }

        private async Task<ToDoListModel> FindTodoListModelById(string todoListId)
        {
            var toDoListModel = await toDoListRepository.FindById(todoListId);
            if (toDoListModel == null)
            {
                throw new NotFoundException(ResourceType.ToDoList);
            }

            return toDoListModel;
        }

        private async Task<ToDoListModel> FindTodoListModelByUserId(string userId)
        {
            var toDoListModel = await toDoListRepository.FindByUserId(userId);

            if (toDoListModel == null)
            {
                throw new NotFoundException(ResourceType.ToDoList);
            }

            return toDoListModel;
        }
    }
}