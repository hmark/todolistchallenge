using System;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Domain.Context;
using Domain.Exceptions;
using Domain.Services.ToDoList;
using Domain.Services.User;
using Domain.ToDo;
using Domain.User;
using Microsoft.EntityFrameworkCore;

namespace Domain.Services.Providers
{
    public class ToDoListProvider
    {
        private readonly Func<DataContext> createDbContext;

        public ToDoListProvider(Func<DataContext> createDbContext)
        {
            this.createDbContext = createDbContext;
        }

        public async Task<ToDoListView[]> GetAll(string adminId)
        {
            using (var db = createDbContext())
            {
                var admin = await db.Users.SingleOrDefaultAsync(x => x.Id == Guid.Parse(adminId));
                if (admin == null)
                {
                    throw new NotFoundException(ResourceType.User);
                }

                if (admin.ClaimType != UserClaimType.Admin)
                {
                    throw new ForbiddenException(ResourceType.ToDo);
                }
                return await db.ToDoList
                    .Include(t => t.ToDos)
                    .Select(x => BuildTodoListView(x))
                    .ToArrayAsync();
            }
        }

        public async Task<ToDoListView> Get(string todoListId)
        {
            using (var db = createDbContext())
            {
                var toDoListEntry = await db.ToDoList
                    .Include(t => t.ToDos)
                    .SingleOrDefaultAsync(t => t.Id == Guid.Parse(todoListId));
                return BuildTodoListView(toDoListEntry);
            }
        }
        
        public async Task<ToDoListView[]> GetByUserId(string userId)
        {
            using (var db = createDbContext())
            {
                return await db.ToDoList
                    .Include(t => t.ToDos)
                    .Where(t => t.UserId == Guid.Parse(userId))
                    .Select(x => BuildTodoListView(x))
                    .ToArrayAsync();
            }
        }
        
        private ToDoListView BuildTodoListView(Entities.ToDoList toDoList)
        {
            if (toDoList == null)
            {
                throw new NotFoundException(ResourceType.ToDoList);
            }
            var toDoViews = toDoList
                .ToDos
                .Select(t => new ToDoView()
                {
                    Id = t.Id.ToString(),
                    Title = t.Title,
                    State = t.State.ToString()
                })
                .ToArray();
            return new ToDoListView
            {
                Id = toDoList.Id.ToString(),
                Title = toDoList.Title,
                ToDoViews = toDoViews
            };
        }
    }
}