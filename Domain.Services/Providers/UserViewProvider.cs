using System;
using System.Linq;
using System.Threading.Tasks;
using Domain.Context;
using Domain.Exceptions;
using Domain.Services.User;
using Domain.User;
using Microsoft.EntityFrameworkCore;

namespace Domain.Services.Providers
{
    public class UserViewProvider
    {
        private readonly Func<DataContext> createDbContext;

        public UserViewProvider(Func<DataContext> createDbContext)
        {
            this.createDbContext = createDbContext;
        }
        
        public async Task<UserView[]> GetAll(string adminId)
        {
            using (var db = createDbContext())
            {
                var dbUser = await db.Users
                    .AsNoTracking()
                    .SingleAsync(x => x.Id == Guid.Parse(adminId));
                if(dbUser.ClaimType != UserClaimType.Admin)
                    throw new ForbiddenException(ResourceType.User);
                var userViews = db.Users
                    .Include(u => u.ToDoList)
                    .ThenInclude(x => x.ToDos)
                    .Select(u => BuildUserView(u))
                    .ToArray();
                return userViews;
            }
        }
        public UserView Get(string userId)
        {
            using (var db = createDbContext())
            {
                var dbUser = db.Users
                    .Include(u => u.ToDoList)
                    .ThenInclude(x => x.ToDos)
                    .AsNoTracking()
                    .SingleOrDefault(x => x.Id == Guid.Parse(userId));
                return BuildUserView(dbUser);
            }
        }
        public async Task<UserView> GetAsync(string userId)
        {
            using (var db = createDbContext())
            {
                var dbUser = await db.Users
                    .Include(u => u.ToDoList)
                    .ThenInclude(x => x.ToDos)
                    .AsNoTracking()
                    .SingleAsync(x => x.Id == Guid.Parse(userId));
                return BuildUserView(dbUser);
            }
        }
        private UserView BuildUserView(Entities.User userView)
        {
            if (userView == null)
            {
                throw new NotFoundException(ResourceType.User);
            }
            var toDoLists = userView
                .ToDoList
                .Select(t => new UserViewToDoList
                {
                    Title = t.Title,
                    ToDos = t.ToDos.Select(x => new UserViewToDo
                    {
                        Title = x.Title,
                        State = x.State.ToString()
                    }).ToArray()
                })
                .ToArray();
            return new UserView
            {
                FirstName = userView.FirstName,
                LastName = userView.LastName,
                Email = userView.Email,
                ToDoLists = toDoLists
            };

        }
    }
}