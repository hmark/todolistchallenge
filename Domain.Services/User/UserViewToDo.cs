namespace Domain.Services.User
{
    public class UserViewToDo
    {
        public string Title { get; set; }
        public string State { get; set; }
    }
}