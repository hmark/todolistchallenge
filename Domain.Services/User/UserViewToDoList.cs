namespace Domain.Services.User
{
    public class UserViewToDoList
    {
        public string Title { get; set; }
        public UserViewToDo[] ToDos { get; set; }
    }
}