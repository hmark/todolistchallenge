namespace Domain.Services.User
{
    public class UserView
    {
        public string FirstName { get;  set; }
        public string LastName { get;  set; }
        public string Email { get;  set; }
        public UserViewToDoList[] ToDoLists { get; set; }
    }
}