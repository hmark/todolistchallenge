using System;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using Microsoft.IdentityModel.Tokens;
using Settings.Helpers;
using Settings.JwtToken;
using Settings.MsSql;

namespace Domain.Services.Token
{
    public interface ITokenProviderService
    {
        string GenerateToken(string userId);
    }

    public class TokenProviderService : ITokenProviderService
    {
        public string GenerateToken(string userId)
        {
            var tokenHandler = new JwtSecurityTokenHandler();
            var tokenSettings = SettingsFileHelper.ReadSettings<JwtTokenSettings>();
            var key = Encoding.ASCII.GetBytes(tokenSettings.SekretKey);
            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new[] 
                {
                    new Claim(ClaimTypes.Name, userId)
                }),
                Expires = DateTime.UtcNow.AddDays(1),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
            };
            var token = tokenHandler.CreateToken(tokenDescriptor);
            return tokenHandler.WriteToken(token);
        }
    }
}