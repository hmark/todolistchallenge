namespace Domain.User
{
    public enum UserModelError
    {
        FirstNameIsNotSpecified,
        LastNameIsNotSpecified,
        EmailIsNotSpecified,
        PasswordIsNotSpecified,
        WrongEmailAddress,
        UnacceptablePassword
    }
}