using Domain.Exceptions;

namespace Domain.User
{
    public class UserModelException : ModelException<UserModelError>
    {
        public UserModelException(UserModelError modelError) : base(modelError)
        {
        }

        public UserModelException(UserModelError[] modelErrors) : base(modelErrors)
        {
        }
    }
}