using System.Collections.ObjectModel;
using Domain.Entities;

namespace Domain.User
{
    public class SaveUserEvent
    {
        public SaveUserEvent(
            string id,
            string firstName,
            string lastName,
            string email,
            string password,
           ToDoList[] toDoList)
        {
            Id = id;
            FirstName = firstName;
            LastName = lastName;
            Email = email;
            Password = password;
            ToDoList = new ReadOnlyCollection<ToDoList>(toDoList ?? new ToDoList[0]);
        }
        public string Id { get;  }
        public string FirstName { get; }
        public string LastName { get;  }
        public string Email { get;  }
        public string Password { get;  }
        public ReadOnlyCollection<ToDoList> ToDoList { get; }
    }
}