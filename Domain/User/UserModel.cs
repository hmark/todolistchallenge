using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Net.Mail;
using Domain.Entities;

namespace Domain.User
{
    public class UserModel
    {
        public string Id { get; private set; }
        public string FirstName { get; private set; }
        public string LastName { get; private set; }
        public string Email { get; private set; }
        public byte[] PasswordHash { get; private set; }
        public byte[] PasswordSalt { get; private set; }
        public UserClaimType ClaimType { get; set; }
        public ReadOnlyCollection<ToDoList> ToDoList { get; private set; }

        public UserModel()
        {
        }

        public static UserModel Load(
            string id,
            string firstName,
            string lastName,
            string email,
            byte[] passwordHash,
            byte[] passwordSalt,
            UserClaimType claimType,
            ToDoList[] toDoList)
        {

            var userModel = new UserModel
            {
                Id = id,
                FirstName = firstName,
                LastName = lastName,
                Email = email,
                PasswordHash = passwordHash,
                PasswordSalt = passwordSalt,
                ClaimType = claimType,
                ToDoList = new ReadOnlyCollection<ToDoList>(toDoList ?? new ToDoList[0])
            };
            return userModel;
        }

        public static UserModel Create(
            string firstName,
            string lastName,
            string email,
            string password)
        {
            var userModelErrors = CheckConstraints(firstName, lastName, email, password);
            if (userModelErrors.Any())
            {
                throw new UserModelException(userModelErrors.ToArray());
            }
            UserPasswordGenerator.Generate(password, out var passwordHash, out var passwordSalt);

            return Load(null, firstName, lastName, email, passwordHash, passwordSalt, UserClaimType.Client, null);
        }
        
        public void Update(
            string firstName,
            string lastName,
            string email,
            string password)
        {
            var userModelErrors = CheckConstraints(firstName, lastName, email, password);
            if (userModelErrors.Any())
            {
                throw new UserModelException(userModelErrors.ToArray());
            }
            UserPasswordGenerator.Generate(password, out var passwordHash, out var passwordSalt);

            FirstName = firstName;
            LastName = lastName;
            Email = email;
            PasswordHash = passwordHash;
            PasswordSalt = passwordSalt;
        }

        public bool HasToDoList() => ToDoList.Any();

        public bool CanCreate() => CanCreateUpdateRemove();

        public bool CanUpdate() => CanCreateUpdateRemove();

        public bool CanRemove() => CanCreateUpdateRemove();

        private static UserModelError[] CheckConstraints(
            string firstName,
            string lastName,
            string email,
            string password)
        {
            var userModelErrors = new List<UserModelError>();

            if (string.IsNullOrWhiteSpace(firstName))
            {
                userModelErrors.Add(UserModelError.FirstNameIsNotSpecified);
            }

            if (string.IsNullOrWhiteSpace(lastName))
            {
                userModelErrors.Add(UserModelError.LastNameIsNotSpecified);
            }

            if (string.IsNullOrWhiteSpace(email))
            {
                userModelErrors.Add(UserModelError.EmailIsNotSpecified);
            }

            if (string.IsNullOrWhiteSpace(password))
            {
                userModelErrors.Add(UserModelError.PasswordIsNotSpecified);
            }

            if (!ValidateEmail(email))
            {
                userModelErrors.Add(UserModelError.WrongEmailAddress);
            }

            if (!ValidatePassword(password))
            {
                userModelErrors.Add(UserModelError.UnacceptablePassword);
            }
            return userModelErrors.ToArray();
        }

        //Could also used RegEx but I personally prefer this approach, this saves me a lot af headaches 
        private static bool ValidateEmail(string emailAddress)
        {
            try
            {
                var mailAddress = new MailAddress(emailAddress);

                return true;
            }
            catch (FormatException)
            {
                return false;
            }
        }

        //Like I said, I don't like regular expressions
        private static bool ValidatePassword(string password)
        {
            const int minLength = 8;
            const int maxLength = 15;

            if (password == null) throw new ArgumentNullException();

            var meetsLengthRequirements = password.Length >= minLength && password.Length <= maxLength;
            var hasUpperCaseLetter = false;
            var hasLowerCaseLetter = false;
            var hasDecimalDigit = false;

            if (meetsLengthRequirements)
            {
                foreach (var c in password)
                {
                    if (char.IsUpper(c)) hasUpperCaseLetter = true;
                    else if (char.IsLower(c)) hasLowerCaseLetter = true;
                    else if (char.IsDigit(c)) hasDecimalDigit = true;
                }
            }

            var isValid = meetsLengthRequirements
                          && hasUpperCaseLetter
                          && hasLowerCaseLetter
                          && hasDecimalDigit;
            return isValid;
        }
        
        private bool CanCreateUpdateRemove() =>
           ClaimType == UserClaimType.Admin;
    }
}