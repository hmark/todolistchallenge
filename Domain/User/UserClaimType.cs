namespace Domain.User
{
    public enum UserClaimType
    {
        Admin = 1,
        Client = 2
    }
}