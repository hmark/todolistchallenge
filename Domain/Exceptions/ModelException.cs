using System;
using System.Linq;

namespace Domain.Exceptions
{
    public class ModelException<TError> : Exception
    {
        public TError[] ModelErrors { get; }

        public ModelException(TError modelError)
            : this(new[] { modelError })
        {
        }

        public ModelException(TError[] modelErrors)
            : base($"Model constraints violated. {nameof(TError)}: {modelErrors.Select(x => x.ToString()).Aggregate((a, b) => a + ", " + b)}")
        {
            ModelErrors = modelErrors;
        }
    }
}