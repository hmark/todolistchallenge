using System;

namespace Domain.Exceptions
{
    public class NotFoundException: Exception
    {
        public ResourceType ResourceType { get; }

        public NotFoundException(ResourceType resourceType): base($"Resource is not found: {resourceType}")
        {
            ResourceType = resourceType;
        }
    }
}