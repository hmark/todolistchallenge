using System;

namespace Domain.Exceptions
{
    public class ForbiddenException : Exception
    {
        public ResourceType ResourceType { get; }

        public ForbiddenException(ResourceType resourceType) : base($"Denied access to the resource: {resourceType}")
        {
            ResourceType = resourceType;
        }
    }
}