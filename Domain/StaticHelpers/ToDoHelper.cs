using System.Linq;
using Domain.ToDo;

namespace Domain.StaticHelpers
{
    public static class ToDoHelper
    {
        
        public static ToDoModel CreateToDoModel(Entities.ToDo todoEntry)
        {
            if (todoEntry == null)
            {
                return null;
            }

            return ToDoModel.Load(
                todoEntry.Id.ToString(),
                todoEntry.Title,
                todoEntry.State,
                todoEntry.ToDoListId.ToString());
        }
        
    }
}