namespace Domain.ToDo
{
    public enum ToDoListModelError
    {
        TitleIsNotSpecified,
        UserIdIsNotSpecified
    }
}