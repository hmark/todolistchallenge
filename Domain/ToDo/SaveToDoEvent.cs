using System.Collections.ObjectModel;

namespace Domain.ToDo
{
    public class SaveToDoEvent
    {
        public string Title { get; }
        public ToDoState ToDoState { get; }
        public string ToDoListId { get; }

        public SaveToDoEvent(string title, ToDoState toDoState, string toDoListId)
        {
            Title = title;
            ToDoState = toDoState;
            ToDoListId = toDoListId;
        }
    }
}