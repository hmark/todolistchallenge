namespace Domain.ToDo
{
    public class ToDoModel
    {
        public string Id { get; private  set; }
        public string Title { get; private set; }
        public ToDoState State { get; private set; }
        public string ToDoListId { get; private set; }
        
        public static ToDoModel Load(
            string id,
            string title,
            ToDoState state,
            string toDoListId)
        {

            var toDoModel = new ToDoModel
            {
                Id = id,
                Title = title,
                State = state,
                ToDoListId = toDoListId
            };
            return toDoModel;
        }
    }
}