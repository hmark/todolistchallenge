using Domain.Exceptions;

namespace Domain.ToDo
{
    public class ToDoListException: ModelException<ToDoListModelError>
    {
        public ToDoListException(ToDoListModelError modelError) : base(modelError)
        {
        }

        public ToDoListException(ToDoListModelError[] modelErrors) : base(modelErrors)
        {
        }
    }
}