using System.Collections.ObjectModel;

namespace Domain.ToDo
{
    public class SaveToDoListEvent
    {
        public string Id { get; }
        public string Title { get; }
        public ReadOnlyCollection<SaveToDoEvent> ToDoModels { get; }
        public string UserId { get; }

        public SaveToDoListEvent(string id, string title, string userId, SaveToDoEvent[] saveToDoEvents)
        {
            Id = id;
            Title = title;
            UserId = userId;
            ToDoModels = new ReadOnlyCollection<SaveToDoEvent>(saveToDoEvents ?? new SaveToDoEvent[0]);
        }
    }
}