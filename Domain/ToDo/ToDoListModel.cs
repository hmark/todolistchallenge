using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Net.Mail;
using Domain.Entities;
using Domain.User;
using Microsoft.EntityFrameworkCore.Internal;

namespace Domain.ToDo
{
    public class ToDoListModel
    {
        public string Id { get; private set; }
        public string Title { get; private set; }
        public ReadOnlyCollection<ToDoModel> ToDoModels { get; private set; }
        public string UserId { get; private set; }

        public ToDoListModel()
        {
        }

        public static ToDoListModel Load(
            string id,
            string title,
            string userId,
            ToDoModel[] toDoModels)
        {

            var toDoListModel = new ToDoListModel
            {
                Id = id,
                Title = title,
                UserId = userId,
                ToDoModels = new ReadOnlyCollection<ToDoModel>(toDoModels ?? new ToDoModel[0])
            };
            return toDoListModel;
        }

        public static ToDoListModel Create(
            string title,
            string userId,
            ToDoModel[] toDoModels)
        {
            var toDoListModelErrors = CheckConstraints(title, userId);
            if (toDoListModelErrors.Any())
            {
                throw new ToDoListException(toDoListModelErrors.ToArray());
            }

            return Load(null, title, userId,  toDoModels);
        }
        
        public void Update(
            string title,
            ToDoModel[] newToDos)
        {
            var toDoListModelErrors = CheckConstraints(title, UserId);
            if (toDoListModelErrors.Any())
            {
                throw new ToDoListException(toDoListModelErrors.ToArray());
            }
            Title = title;
            if (newToDos.Any())
            {
                ToDoModels = new ReadOnlyCollection<ToDoModel>(newToDos);
            }
        }

        public bool HasToDoList() => ToDoModels.Any();

        public bool CanUpdate(string updaterId) => CanUpdateRemove(updaterId);

        public bool CanRemove(string removerId) => CanUpdateRemove(removerId);

        private static ToDoListModelError[] CheckConstraints(
            string title,
            string userId)
        {
            var toDoListModelErrors = new List<ToDoListModelError>();

            if (string.IsNullOrWhiteSpace(title))
            {
                toDoListModelErrors.Add(ToDoListModelError.TitleIsNotSpecified);
            }

            if (string.IsNullOrWhiteSpace(userId))
            {
                toDoListModelErrors.Add(ToDoListModelError.UserIdIsNotSpecified);
            }

            return toDoListModelErrors.ToArray();
        }

        
        private bool CanUpdateRemove(string userid) =>
           userid.Equals(UserId);
    }
}