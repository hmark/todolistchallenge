namespace Domain.ToDo
{
    public enum ToDoState : byte
    {
        Open = 1,
        Closed = 2
    }
}