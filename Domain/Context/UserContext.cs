using System;
using Domain.Entities;
using Domain.User;
using Microsoft.EntityFrameworkCore;

namespace Domain.Context
{
    public class DataContext : DbContext
    {
        public DataContext(DbContextOptions<DataContext> options) : base(options) { }
 
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            UserPasswordGenerator.Generate("Passw0rd!", out var passwordHash, out var passwordSalt);

            modelBuilder.Entity<Entities.User>().HasData(new Entities.User
            {
                Id = Guid.NewGuid(),
                FirstName = "Admin",
                LastName = "Adminyan",
                Email = "admin@devlix.de",
                ClaimType = UserClaimType.Admin,
                PasswordHash = passwordHash,
                PasswordSalt = passwordSalt,
            });
        }
        public DbSet<Entities.User> Users { get; set; }
        public DbSet<Entities.ToDo> ToDo { get; set; }
        public DbSet<ToDoList> ToDoList { get; set; }
    }
}