using System;
using System.Collections;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Domain.ToDo;

namespace Domain.Entities
{
    public class ToDo
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid Id { get; set; }

        public string Title { get; set; }
        public ToDoState State { get; set; }
        [ForeignKey("ToDoList")]
        public Guid ToDoListId { get; set; }
        public ToDoList ToDoList { get; set; }
        
    }
}