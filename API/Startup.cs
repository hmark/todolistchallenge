﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using Domain.Context;
using Domain.Services.PasswordVerification;
using Domain.Services.Providers;
using Domain.Services.Repositories;
using Domain.Services.Services;
using Domain.Services.Token;
using Domain.ToDo;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json;
using Settings;
using Settings.Helpers;
using Settings.JwtToken;
using Settings.MsSql;
using Swashbuckle.AspNetCore.Swagger;

namespace API
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            ConfigureAspNet(services);
            ConfigureDb(services);
            ConfigureDomainServices(services);
            ConfigureRepositories(services);
            ConfigureAuthentication(services);
            ConfigureSwagger(services);
        }

        
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            using (var serviceScope = app.ApplicationServices.GetService<IServiceScopeFactory>().CreateScope())
            {
                var context = serviceScope.ServiceProvider.GetRequiredService<DataContext>();
                context.Database.EnsureCreated();
            }
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseHsts();
            }
            app.UseHttpsRedirection();
            app.UseCors(x => x
                .AllowAnyOrigin()
                .AllowAnyMethod()
                .AllowAnyHeader()
                .AllowCredentials());
 
            app.UseAuthentication();
            // Enabling middleware to serve generated Swagger as a JSON endpoint.
            app.UseSwagger(); 
            // Enabling middleware to serve swagger-ui specifying the Swagger JSON endpoint.
            app.UseSwaggerUI(c => { c.SwaggerEndpoint("/swagger/v1/swagger.json", "My API V1"); });
            app.UseMvc();
        }

        private static void ConfigureSwagger(IServiceCollection services)
        {
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new Info { Title = "ToDo API", Version = "v1" });
            });

        }
        
        private static void ConfigureAspNet(IServiceCollection services)
        {
            services.AddCors();
            services
                .AddMvc(config => { config.InputFormatters.Add(new CustomInputFormatter()); })
                .AddJsonOptions(options =>
                {
                    options.SerializerSettings.ReferenceLoopHandling = ReferenceLoopHandling.Ignore;
                    options.SerializerSettings.Formatting = Formatting.Indented;
                })
                .AddXmlSerializerFormatters();
            services.AddAutoMapper();
        }

        private void ConfigureAuthentication(IServiceCollection services)
        {
            var jwtTokenSettings = SettingsFileHelper.ReadSettings<JwtTokenSettings>();
            var key = Encoding.ASCII.GetBytes(jwtTokenSettings.SekretKey);
            services.AddAuthentication(x =>
                {
                    x.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                    x.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
                })
                .AddJwtBearer(x =>
                {
                    x.Events = new JwtBearerEvents
                    {
                        OnTokenValidated = context =>
                        {
                            var userViewProvider = context
                                .HttpContext.RequestServices
                                .GetRequiredService<UserViewProvider>();
                            var userId = context.Principal.Identity.Name;
                            var user = userViewProvider.Get(userId);
                            if (user == null)
                            {
                                context.Fail("Unauthorized");
                            }
                            return Task.CompletedTask;
                        }
                    };
                    x.RequireHttpsMetadata = false;
                    x.SaveToken = true;
                    x.TokenValidationParameters = new TokenValidationParameters
                    {
                        ValidateIssuerSigningKey = true,
                        IssuerSigningKey = new SymmetricSecurityKey(key),
                        ValidateIssuer = false,
                        ValidateAudience = false
                    };
                });
            
        }
        
        private void ConfigureDomainServices(IServiceCollection services)
        {
            services.AddScoped<UserService>();
            services.AddScoped<ToDoService>();
            services.AddSingleton<UserViewProvider>();
            services.AddSingleton<ToDoListProvider>();
            services.AddScoped<IPasswordVerificationService, PasswordVerificationService>();
            services.AddScoped<ITokenProviderService, TokenProviderService>();
        }

        private void ConfigureRepositories(IServiceCollection services)
        {
            services.AddSingleton<IToDoListRepository, ToDoListRepository>();
            services.AddSingleton<IUserModelRepository, UserModelRepository>();
        }
        private void ConfigureDb(IServiceCollection services)
        {
            services.AddDbContext<DataContext>(builder =>
            {
                var sqlSettings = SettingsFileHelper.ReadSettings<MsSqlSettings>();
                builder.UseSqlServer(sqlSettings.ConnectionString, b => b.MigrationsAssembly("API"));
            }, ServiceLifetime.Singleton);
            services.AddTransient<DataContext>();
            services.AddSingleton(provider => new Func<DataContext>(provider.GetService<DataContext>));
        }
        
    }
}