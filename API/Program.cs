﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Serilog;
using Serilog.Events;

namespace API
{
    public class Program
    {
        public static void Main(string[] args)
        {
            ConfigureSerilogLogging();
            CreateWebHostBuilder(args).Build().Run();
        }

        public static IWebHostBuilder CreateWebHostBuilder(string[] args) =>
            WebHost.CreateDefaultBuilder(args)
                .UseStartup<Startup>()
                .UseUrls("http://localhost:4000");
        
        private static void ConfigureSerilogLogging()
        {
            Log.Logger = new LoggerConfiguration()
                .MinimumLevel.Verbose()
                //.MinimumLevel.Override("System", LogEventLevel.Warning)
                //.MinimumLevel.Override("Microsoft", LogEventLevel.Warning)
                .MinimumLevel.Override("Microsoft.EntityFrameworkCore", LogEventLevel.Warning)
                .WriteTo.Logger(config =>
                    config
                        .Filter.ByIncludingOnly(x => x.Level >= LogEventLevel.Debug)
                        .WriteTo.RollingFile(@"C:\logs\todoApi\log-{Date}-debug.txt",
                            flushToDiskInterval: TimeSpan.FromSeconds(5),
                            buffered: true,
                            outputTemplate: "{Timestamp:HH:mm:ss.fff} [{Level:u4}] [{SourceContext}] {Message:lj}{NewLine}{Exception}")
                )
                .WriteTo.Logger(config =>
                    config
                        .Filter.ByIncludingOnly(x => x.Level >= LogEventLevel.Information)
                        .WriteTo.RollingFile(@"C:\logs\todoApi\log-{Date}-info.txt",
                            flushToDiskInterval: TimeSpan.FromSeconds(5),
                            buffered: true,
                            outputTemplate: "{Timestamp:HH:mm:ss.fff} [{Level:u4}] [{SourceContext}] {Message:lj}{NewLine}{Exception}")
                )
                .WriteTo.Logger(config =>
                    config
                        .Filter.ByIncludingOnly(x => x.Level >= LogEventLevel.Error)
                        .WriteTo.RollingFile(@"C:\logs\todoApi\log-{Date}-error.txt",
                            flushToDiskInterval: TimeSpan.FromSeconds(2),
                            buffered: true,
                            outputTemplate: "{Timestamp:HH:mm:ss.fff} [{Level:u4}] [{SourceContext}] {Message:lj}{NewLine}{Exception}")
                )
                .CreateLogger();
        }
    }
}