using System.ComponentModel.DataAnnotations;

namespace API.ViewModels
{
    public class SaveTodoListViewModel
    {
        [Required]
        [MaxLength(100)]
        public string Title { get; set; }
        public SaveTodoViewModel[] Todos { get; set; }
    }
}