using System.ComponentModel.DataAnnotations;

namespace API.ViewModels
{
    public class SaveTodoViewModel
    {
        [Required]
        [MaxLength(100)]
        public string Title { get; set; }
    }
}