using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using API.Extensions;
using API.Filters;
using API.ViewModels;
using Domain.Exceptions;
using Domain.Services.PasswordVerification;
using Domain.Services.Providers;
using Domain.Services.Services;
using Domain.ToDo;
using Domain.User;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace API.Controllers
{
    [Route("[controller]")]
    [ApiController]
    [Authorize]
    public class ToDoListController : Controller
    {
        private readonly ToDoService toDoService;
        private readonly ToDoListProvider toDoListProvider;
        private readonly ILogger<ToDoListController> logger;
 

        public ToDoListController(
            ToDoService toDoService, 
            ToDoListProvider toDoListProvider,
            ILogger<ToDoListController> logger)
        {
            this.toDoService = toDoService;
            this.toDoListProvider = toDoListProvider;
            this.logger = logger;
        }
        //This method is for admins only
        [HttpGet]
        public async Task<IActionResult> Get()
        {
            return await HandleException(async () =>
            {
                var userId = User.GetUserId();
                var toDoListView = await toDoListProvider.GetAll(userId);
                return Json(toDoListView);
            }, "Something went wrong :(");
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> Get(string id)
        {
            return await HandleException(async () =>
            {
                var toDoListView = await toDoListProvider.Get(id);
                return Json(toDoListView);
            }, new {id});
        }
        
        [HttpGet]
        [Route("List/{userId}")]
        public async Task<IActionResult> List(string userId)
        {
            return await HandleException(async () =>
            {
                var toDoListView = await toDoListProvider.GetByUserId(userId);
                return Json(toDoListView);
            }, new {userId});
        }

        // POST api/values
        [HttpPost]
        [ValidateModel]
        public async Task<IActionResult> Post([FromBody] SaveTodoListViewModel todoListViewModel)
        {
            return await HandleException(async () =>
            {
                var userId =  User.GetUserId();
                
                var saveToDoEvents = todoListViewModel.Todos
                    .Select(x => new SaveToDoEvent(x.Title, ToDoState.Open, null))
                    .ToArray();
                var saveToDoListEvent = new SaveToDoListEvent(null, todoListViewModel.Title, userId, saveToDoEvents);
                await toDoService.SaveTodoList(saveToDoListEvent);
                return Ok();
            }, new {todoListViewModel});

        }
        [HttpPost]
        [ValidateModel]
        [Route("AddList")]
        public async Task<IActionResult> AddList([FromBody] SaveTodoListViewModel[] todoListViewModel)
        {
            return await HandleException(async () =>
            {
                var userId =  User.GetUserId();
                await toDoService.SaveTodoList((from listViewModel in todoListViewModel
                    let saveToDoEvents = listViewModel.Todos.Select(x => new SaveToDoEvent(x.Title, ToDoState.Open, null))
                        .ToArray()
                    select new SaveToDoListEvent(null, listViewModel.Title, userId, saveToDoEvents)).ToArray());
                return Ok();
            }, new {todoListViewModel});

        }

        // PUT api/values/5
        [HttpPut("{id}")]
        [ValidateModel]
        public async Task<IActionResult> Put(string id, [FromBody] SaveTodoListViewModel todoListViewModel)
        {
            return await HandleException(async () =>
            {
                var userId =  User.GetUserId();
                
                var saveToDoEvents = todoListViewModel.Todos
                    .Select(x => new SaveToDoEvent(x.Title, ToDoState.Open, id))
                    .ToArray();
                var saveToDoListEvent = new SaveToDoListEvent(id, todoListViewModel.Title, userId, saveToDoEvents);
                await toDoService.SaveTodoList(saveToDoListEvent);
                return Ok();
            }, new {todoListViewModel});
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(string id)
        {
            return await HandleException(async () =>
            {
                var userId =  User.GetUserId();
                await toDoService.Remove(userId, id);
                return Ok();
            }, new {id});
        }
        
        private async Task<IActionResult> HandleException(Func<Task<IActionResult>> action, object logData)
        {
            void LogException(Exception exception)
            {
                logger.LogError(default(EventId), exception, "{message}, {@logData}", exception.Message, logData);
            }

            try
            {
                return await action();
            }
            catch (UserModelException e)
            {
                LogException(e);
                return BadRequest(new
                {
                    type = "userModel",
                    data = e.ModelErrors
                        .Select(error => error.ToString())
                        .ToArray()
                });
            }
            catch (PasswordVerificationFailedException e)
            {
                LogException(e);
                return BadRequest(new
                {
                    type = "passwordVerification",
                    data = e.ErrorCode
                });
            }
            catch (ForbiddenException e)
            {
                LogException(e);
                HttpContext.Response.StatusCode = StatusCodes.Status403Forbidden;
                return Json(e.ResourceType);
            }
            catch (NotFoundException e)
            {
                LogException(e);
                return NotFound(e.ResourceType);
            }
            catch (Exception e)
            {
                LogException(e);
                throw e;
            }
        }
    }
}