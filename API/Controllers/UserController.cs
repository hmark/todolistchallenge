﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using API.Extensions;
using API.Filters;
using API.ViewModels;
using Domain.Exceptions;
using Domain.Services.PasswordVerification;
using Domain.Services.Providers;
using Domain.Services.Services;
using Domain.Services.Token;
using Domain.User;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace API.Controllers
{
    [Route("[controller]")]
    [ApiController]
    [Authorize]
    public class UserController : Controller
    {
        private readonly UserService userService;
        private readonly UserViewProvider userViewProvider;
        private readonly ILogger<UserController> logger;

        public UserController(
            UserService userService,
            UserViewProvider userViewProvider,
            ILogger<UserController> logger)
        {
            this.userService = userService;
            this.userViewProvider = userViewProvider;
            this.logger = logger;
        }

        [AllowAnonymous]
        [ValidateModel]
        [HttpPost("authenticate")]
        public async Task<IActionResult> Authenticate([FromBody] LoginViewModel loginViewModel)
        {
            return await HandleException(async () =>
            {
                var token = await userService.Authenticate(loginViewModel.UserName, loginViewModel.Password);
                return Ok(new
                {
                    Token = token
                });
            }, new {loginViewModel});
        }

        [HttpGet]
        public async Task<IActionResult> Get()
        {
            var userId =  User.GetUserId();
            var userViews = await userViewProvider.GetAll(userId);
            return Json(userViews);

        }

        // GET api/values/5
        [HttpGet("{id}")]
        public async Task<IActionResult> Get(string id)
        {
            var userViews = await userViewProvider.GetAsync(id);
            return Json(userViews);
        }

        // POST api/values
        [HttpPost]
        [ValidateModel]
        public async Task<IActionResult>  Post([FromBody] SaveUserViewModel saveUser)
        {
            return await SaveUser(null, saveUser);
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        [ValidateModel]
        public async Task<IActionResult> Put(string id, [FromBody] SaveUserViewModel saveUser)
        {
            return await SaveUser(id, saveUser);
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(string id)
        {
            return await HandleException(async () =>
            {
                var userId =  User.GetUserId();
                await userService.Remove(userId, id);
                return Ok();
            }, new {id});
        }

        private async Task<IActionResult> SaveUser(string id, SaveUserViewModel saveUser)
        {
            return await HandleException(async () =>
            {
                var saveUserEvent = new SaveUserEvent(id, saveUser.FirstName, saveUser.LastName, saveUser.Email,
                    saveUser.Password, null);
                var userId =  User.GetUserId();
                await userService.SaveUser(saveUserEvent, userId);
                return Ok();
            }, new {saveUser});
        }
        
        private async Task<IActionResult> HandleException(Func<Task<IActionResult>> action, object logData)
        {
            void LogException(Exception exception)
            {
                logger.LogError(default(EventId), exception, "{message}, {@logData}", exception.Message, logData);
            }

            try
            {
                return await action();
            }
            catch (UserModelException e)
            {
                LogException(e);
                return BadRequest(new
                {
                    type = "userModel",
                    data = e.ModelErrors
                        .Select(error => error.ToString())
                        .ToArray()
                });
            }
            catch (PasswordVerificationFailedException e)
            {
                LogException(e);
                return BadRequest(new
                {
                    type = "passwordVerification",
                    data = e.ErrorCode
                });
            }
            catch (ForbiddenException e)
            {
                LogException(e);
                HttpContext.Response.StatusCode = StatusCodes.Status403Forbidden;
                return Json(e.ResourceType);
            }
            catch (NotFoundException e)
            {
                LogException(e);
                return NotFound(e.ResourceType);
            }
            catch (Exception e)
            {
                LogException(e);
                throw e;
            }
        }
    }
}